"""Contains the abstract and base objects for the pyDanceWall
"""

import numpy as np
import cv2

class DWObject():
	"""This is the abstract object with some common functions.
	"""
	def __init__(self, debug=False):
		self.className = self.__class__.__name__
		self.debug = debug

	def turnOffDebug(self):
		self.debug = False

	def turnOnDebug(self):
		self.debug = True

	def toggleDebug(self):
		self.debug = not self.debug

	def printDebugMsg(self, msg):
		if self.debug:
			print('Debug msg in {0}: {1}'.format(self.className, msg))

	def printError(self, msg):
		print('Error in {0}: {1}'.format(self.className, msg))

	def showDebugImage(self, name, image):
		if self.debug:
			windowname = self.className + '::' + name
			cv2.imshow( windowname, image)

	def nextOneWayParm(self, curValue, maxVal, minVal=0, increment=1):
		""" This function is used to step up a value until a maximum
		and then reset to the minimum value.)
		"""

		# sanity check.
		if maxVal <= minVal:
			self.printError('Max/Min parameters in nextOneWayParm are invalid.')
			return curValue

		# increment and check for ceiling.
		curValue += increment
		if(curValue >= maxVal):
			curValue = minVal

		return curValue

	def nextTwoWayParm(self, curValue, curRate, minVal, maxVal):
		""" This function is used to cycle a parameter thorugh equal steps
		up and down.
		"""

		# sanity check.
		if maxVal <= minVal:
			self.printError('Max/Min parameters in nextTwoWayParm are invalid.')
			return curValue, curRate

		# change the current value by the linear rate.
		curValue += curRate

		# check for a change in the rate direction.
		if curValue <= minVal:
			curValue = minVal
			curRate *= -1
		elif curValue >= maxVal:
			curValue = maxVal
			curRate *= -1

		return curValue, curRate
