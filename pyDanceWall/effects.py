""" These are the effects being applied.

"""
import os

from math import floor, sqrt, atan2, sin, cos, exp, log
from time import time
import random
import numpy as np
import cv2
import pickle

from .base import DWObject

MAX_COLORMAPS = 12
MAX_GRIDORDER = 10

# for ColorEffect
BLUE_CHANNEL = 0
GREEN_CHANNEL = 1
RED_CHANNEL = 2
COLOR_FULL = 3
GRAYSCALE = 4

# for WarpEffect
MAXWARPEFFECTS = 4
HORIZONTAL_WARP = 0
VERTICAL_WARP = 1
HV_WARP = 2
SWIRL_WARP = 3

class AbstractEffect(DWObject):
    """This is the abstract class which most effect classes are derived.
    """

    def __init__(self, debug=False):
        super().__init__(debug)

        # image variables
        self.lastFrame = None
        self.outFrame = None

        # set-up colormap variables
        self.currentColorMap = 0
        self.bApplyColorMap = False
        self.randColorMapList = []

        # set-up the grid order, e.g. how many of the same image.
        self.gridOrder = 1

    def addNewFrame(self, frame):
        """Copy in the latest frame from video camera.
        """
        # check frame type.
        if not isinstance(frame, np.ndarray):
            msg = 'Invalid image type passed in -- expecting numpy.ndarray'
            raise ValueError(msg)

        self.lastFrame = frame

    def drawEffect(self):
        """Draw the intended effect <-- Needs to be implemented in the sub-class.
        """
        msg = 'This function should never called.  You need to reimplement in the subclass.'
        self.printError(msg)

    def getOutFrame(self):
        """Obtain the latest effect graphic.
        """

        # simple full image.
        if self.gridOrder == 1:
            if self.bApplyColorMap:
                self.outFrame = cv2.applyColorMap(self.outFrame, self.currentColorMap)
            return self.outFrame

        # otherwise, we build a grid.
        nImages = self.gridOrder * self.gridOrder

        # what is the full image size?
        fullImageHeight = self.outFrame.shape[0]
        fullImageWidth = self.outFrame.shape[1]
        #channels = self.outFrame[2]

        # what is the reduced image size?
        subImageHeight = int(fullImageHeight/self.gridOrder)
        subImageWidth = int(fullImageWidth/self.gridOrder)

        # build the small image that will be repeated.
        baseImg = cv2.resize(self.outFrame, (subImageWidth, subImageHeight))
        if len(baseImg.shape) != 3:
            baseImg = baseImg[:, :, None]

        # set the outgoing frame to black.
        self.setOutFrameToBlack()

        # add image to the grid.
        for i in range(nImages):
            # determine the x,y location in grid.
            x0 = int((i % self.gridOrder) * subImageWidth)
            x1 = int(x0 + subImageWidth)
            y0 = int(floor(i / self.gridOrder) * subImageHeight)
            y1 = int(y0 + subImageHeight)

            tempImg = baseImg.copy()

            if self.bApplyColorMap:
                tempImg = cv2.applyColorMap(tempImg, self.randColorMapList[i])

            self.outFrame[y0:y1, x0:x1, :] = tempImg.copy()

        return self.outFrame

    def togglePresets(self):
        """Toggle to the next set of presets. <-- Needs to be implemented in the sub-class.
        """
        msg = 'This function should never called.  You need to reimplement in the subclass.'
        self.printError(msg)

    def changeColorMap(self):
        """Toggle through the colormaps.
        """
        self.currentColorMap = self.nextOneWayParm(self.currentColorMap, MAX_COLORMAPS)

    def toggleColorMapsOn(self):
        """Toggle the on/off switch.
        """
        self.bApplyColorMap = not self.bApplyColorMap

    def setColorMapApply(self, bColorMapApply=True):
        """Set whether we are going to sue the color map.  Default = True
        """
        self.bApplyColorMap = bColorMapApply

    def setOutFrameToBlack(self):
        """Set the outframe to the same size as last and fully black.
        """

        # create a black background for building new images.
        cols = self.lastFrame.shape[0]
        rows = self.lastFrame.shape[1]
        self.outFrame = np.zeros((cols, rows, 3), np.uint8)

    def setGridOrder(self, order=1):
        """Set the size of grid used in output grid effect.
        """
        if order < 1:
            msg = 'setGridOrder(): Grid order was set to an invalid value. Setting to default=1'
            self.printError(msg)
            self.gridOrder = 1
        else:
            self.gridOrder = order

        self.computeRandomColorMapList()

    def incrementGridOrder(self):
        """Increment the grid order by one up to maximum of 10.
        """
        self.gridOrder = self.nextOneWayParm(self.gridOrder, MAX_GRIDORDER, 1)
        self.computeRandomColorMapList()

    def computeRandomColorMapList(self):
        """Create a random list of color maps for the grid effect.
        """
        size = self.gridOrder * self.gridOrder
        lst = []

        for i in range(size):
            lst.append(random.randint(0, MAX_COLORMAPS-1))

        self.randColorMapList = lst.copy()


    def getRandomConfig(self, doGrid=False):
        """Generate a random effect as part.
        """
        print('getRandomConfig base class: {0}'.format(doGrid))

        # if we want a grid, what order?
        if doGrid:
            self.setGridOrder(random.randint(2, MAX_GRIDORDER-1))
        else:
            self.setGridOrder(1)

        # do want the color maps?
        randvar = random.random() # random number [0.0, 1.0)
        if randvar < 0.7:
            # apply the color maps.
            self.setColorMapApply(True)
            self.currentColorMap = random.randint(0, MAX_COLORMAPS-1)
        else:
            self.setColorMapApply(False)

class ColorEffect(AbstractEffect):
    """Simple effect that just changes the colour channels.
    """
    def __init__(self, debug=False):
        super().__init__(debug)

        self.colorType = 0

        self.init()

    def init(self):
        """Initialization that can be called anytime the effect is reset.
        """
        self.colorType = COLOR_FULL

    def drawEffect(self):
        """Draw the intended effect.
        Do not call the parent class.
        """

        # black and white
        if self.colorType == GRAYSCALE:
            self.outFrame = cv2.cvtColor(self.lastFrame, cv2.COLOR_BGR2GRAY)
        elif self.colorType == BLUE_CHANNEL:
            self.outFrame = self.lastFrame.copy()
            self.outFrame[:, :, 1] = 0
            self.outFrame[:, :, 2] = 0
        elif self.colorType == GREEN_CHANNEL:
            self.outFrame = self.lastFrame.copy()
            self.outFrame[:, :, 0] = 0
            self.outFrame[:, :, 2] = 0
        elif self.colorType == RED_CHANNEL:
            self.outFrame = self.lastFrame.copy()
            self.outFrame[:, :, 0] = 0
            self.outFrame[:, :, 1] = 0
        else:
            self.outFrame = self.lastFrame.copy()

    def setColorType(self, colorType):
        """Explicitly set the color type.
        """
        self.colorType = colorType

    def getColorType(self):
        """Return the current color type.
        """
        return self.colorType

    def togglePresets(self):
        """Toggle to the next set of presets.
        Do not call the parent class.
        """
        self.colorType = self.nextOneWayParm(self.colorType, GRAYSCALE+1)

    def getRandomConfig(self, doGrid=False):
        super().getRandomConfig(doGrid)
        print('getRandomConfig child class: {0}'.format(doGrid))

        self.setColorType(random.randint(0, COLOR_FULL))

class CartoonEffect(AbstractEffect):
    """Effect that creates a cartoon look and feel.
       Credit: http://www.askaswiss.com/2016/01/how-to-create-cartoon-effect-opencv-python.html
    """
    def __init__(self, debug=False):
        super().__init__(debug)

        self.num_down = 2
        self.num_bilateral = 7
        self.blur_kernel_size = 7

        self.init()

    def init(self):
        """Initialization that can be called anytime the effect is reset.
        """
        pass

    def drawEffect(self):
        """Draw the intended effect.
        Do not call the parent class.
        """

        # downsample image using Gaussian pyramid.
        color_frame = self.lastFrame
        for _ in range(self.num_down):
            color_frame = cv2.pyrDown(color_frame)

        # repeatedly apply small bilateral filter instead of applying one large filter.
        for _ in range(self.num_bilateral):
            color_frame = cv2.bilateralFilter(color_frame, d=9, sigmaColor=9, sigmaSpace=7)

        # upsample image to original size.
        for _ in range(self.num_down):
            color_frame = cv2.pyrUp(color_frame)

        # convert to grayscale and apply median blur.
        bw_frame = cv2.cvtColor(self.lastFrame, cv2.COLOR_RGB2GRAY) # I have BGR2GRAY in C++
        blur_frame = cv2.medianBlur(bw_frame, self.blur_kernel_size)

        # detect and enhance edges.
        edge_frame = cv2.adaptiveThreshold(blur_frame, 255, 
                                           cv2.ADAPTIVE_THRESH_MEAN_C,
                                           cv2.THRESH_BINARY,
                                           blockSize=9,
                                           C=2)

        # convert back to color.
        edge_frame = cv2.cvtColor(edge_frame, cv2.COLOR_GRAY2RGB)

        # combine two images.
        self.outFrame = cv2.bitwise_and(color_frame, edge_frame) 

    def togglePresets(self):
        """Toggle to the next set of presets.
        Do not call the parent class.
        """
        pass

    def getRandomConfig(self, doGrid=False):
        super().getRandomConfig(doGrid)
        print('getRandomConfig child class: {0}'.format(doGrid))

        #if doing grid, always apply color map.
        if self.gridOrder > 1:
            self.bApplyColorMap = True

class EdgeEffect(AbstractEffect):
    """Effect that computes the edges and displays them with different colour maps.
    """
    def __init__(self, debug=False):
        super().__init__(debug)

        self.init()

    def init(self):
        """Initialization that can be called anytime the effect is reset.
        """
        self.blurKernelSize = 3
        self.cannyThreshold1 = 10
        self.cannyThreshold2 = 30
        self.cannyAppertureSize = 3

    def drawEffect(self):
        """Draw the intended effect.
        Do not call the parent class.
        """

        # convert to black and white
        bwFrame = cv2.cvtColor(self.lastFrame, cv2.COLOR_BGR2GRAY)

        # blur image and call the Canny edge detector
        self.outFrame = cv2.blur(bwFrame, (self.blurKernelSize, self.blurKernelSize))
        self.outFrame = cv2.Canny(
            self.outFrame, self.cannyThreshold1, self.cannyThreshold2, self.cannyAppertureSize)

    def togglePresets(self):
        """Toggle to the next set of presets.
        Do not call the parent class.
        """
        self.bApplyColorMap = not self.bApplyColorMap
        self.setColorMapApply(self.bApplyColorMap)

    def getRandomConfig(self, doGrid=False):
        super().getRandomConfig(doGrid)
        print('getRandomConfig child class: {0}'.format(doGrid))

        #if doing grid, always apply color map.
        if self.gridOrder > 1:
            self.bApplyColorMap = True

class WarpEffect(AbstractEffect):
    """Effect that computes the edges and displays them with different colour maps.
    """
    def __init__(self, debug=False):
        super().__init__(debug)

        self.load_warp_maps()
        self.init()

    def init(self):
        """Initialization that can be called anytime the effect is reset.
        """

        self.currentColorMap = 4
        self.currentWarpType = 0
        self.init_map_index()
        return

    def drawEffect(self):
        """Draw the intended effect.
        Do not call the parent class.
        """

        # do some benchmarking.
        t0 = time()

        # convert to black and white
        bwFrame = cv2.cvtColor(self.lastFrame, cv2.COLOR_BGR2GRAY)

        # apply the color map.
        colFrame = cv2.applyColorMap(bwFrame, self.currentColorMap)

        t1 = time()

        maps = self.warp_maps[self.currentWarpType]
        srcX = maps[self.cur_map_index][0]
        srcY = maps[self.cur_map_index][1]

        self.cur_map_index, self.cur_map_rate = self.nextTwoWayParm(self.cur_map_index, 
                                    self.cur_map_rate, 0, self.number_warp_maps-1)

        #print(colFrame.shape)

        t2 = time()
        self.outFrame = cv2.remap(colFrame, srcX, srcY, cv2.INTER_LINEAR)

        t3 = time()

        #print('Prelim time: {0} sec -- Map Gen time: {1} sec -- Remap time: {2} sec'.format(t1-t0, t2-t1, t3-t2))

    def togglePresets(self):
        """Toggle to the next set of presets.
        Do not call the parent class.
        """
        self.currentWarpType = self.nextOneWayParm(self.currentWarpType, MAXWARPEFFECTS)
        self.init_map_index()

    def getRandomConfig(self, doGrid=False):
        super().getRandomConfig(doGrid)
        print('getRandomConfig child class: {0}'.format(doGrid))

        # get random warp type.
        self.currentWarpType = random.randint(0, MAXWARPEFFECTS-1)
        self.init_map_index()

    def load_warp_maps(self):
        """ Load the warp maps from file.
        """
        # TODO: add a check on the valid file and perhaps call the map ge
        warp_filename = 'util/warp_map_{0}x{1}.pickle'.format(640,480)
        warp_filepath = os.path.join(os.path.dirname(__file__), warp_filename)
        warp_file = open(warp_filepath, 'rb')
        self.warp_maps = pickle.load(warp_file)
        warp_file.close()

    def init_map_index(self):
        """ This will find out the number of maps in the map object and init to the middle.
        """
        self.number_warp_maps = len(self.warp_maps[self.currentWarpType])
        self.cur_map_index = floor(self.number_warp_maps/2)
        self.cur_map_rate = 1
