"""This is a script to create the warp maps needed for the warp effect.
"""
import sys
import platform
from math import floor, sqrt, atan2, sin, cos, exp, log
from time import time
import pickle

import numpy as np
import cv2

from pyDanceWall.effects import MAXWARPEFFECTS, HORIZONTAL_WARP, VERTICAL_WARP, HV_WARP, SWIRL_WARP

# each image shape contains a tuple with image width, image height and radius (for swirl)
img_shapes = {'640x480':(640, 480, 240)}
wave_magnitude = 10.0
swirl_rotation = 1
warp_values = dict()
warp_values[HORIZONTAL_WARP] = np.arange(2.0, 15.1, 0.1)
warp_values[VERTICAL_WARP] = np.arange(2.0, 15.1, 0.1)
warp_values[HV_WARP] = np.arange(2.0, 15.1, 0.1)
warp_values[SWIRL_WARP] = np.arange(-2.0, 2.05, 0.05)

def main(args=None):
    """The main routine."""
    if args is None:
        args = sys.argv[1:]

    warp_maps = dict()

    # loop through all the image sizes to create maps.
    for img_shape in img_shapes:
        print('Preparing maps for {0}...'.format(img_shape))
        
        # build some of the local properties.
        cols = img_shapes[img_shape][0]
        rows = img_shapes[img_shape][1]
        radius = img_shapes[img_shape][2] 
        print('Image size: {0} x {1}   radius: {2}'.format(cols, rows, radius))

        for warp_type in range(MAXWARPEFFECTS):
            # e.g. loop through each warp effect to create the maps.
            print('Building warp effect {0}'.format(warp_type))
            #initialize warp map.
            warp_maps[warp_type] = compute_warp_map(warp_type, warp_values[warp_type], rows, cols, radius)

        # write out the warp_map.
        filename = 'warp_map_{0}.pickle'.format(img_shape)
        outfile = open(filename, 'wb')
        pickle.dump(warp_maps, outfile)
        outfile.close()

def compute_warp_map(warp_type, warp_vals, rows, cols, radius):
    # define the image origin
    ci = rows/2.0
    cj = cols/2.0

    # set-up swirl radius.
    swirlRadius = (log(2) * radius) / 1

    # compute the effect boundary defined by rho
    rhoBound = 1.0 * min(ci, cj)

    cur_warp_maps = []

    for warp_val in warp_vals:
        
        # create the map functions, e.g. a numpy array.
        srcX = np.zeros((rows, cols), np.float32)
        srcY = np.zeros((rows, cols), np.float32)

        # create the mapping
        for i in range(rows):
            for j in range(cols):
                # get the cartesian coordinates
                x = i - ci
                y = j - cj

                # get the polar coordinates
                rho = sqrt(x*x + y*y)
                phi = atan2(y, x)

                if rho > rhoBound:
                    # outside bound, don't apply the filter
                    srcX[i, j] = float(j) # remain on same column
                    srcY[i, j] = float(i) # remain on same row
                else:
                    rhoFactor = 1 - pow(rho, 2)/pow(rhoBound, 2)

                    if warp_type == HORIZONTAL_WARP:
                        period = warp_val
                        mx = float(j)
                        my = i + rhoFactor * wave_magnitude * sin(j/period)

                    elif warp_type == VERTICAL_WARP:
                        period = warp_val
                        mx = j + rhoFactor * wave_magnitude * sin(i/period)
                        my = float(i)

                    elif warp_type == HV_WARP:
                        period = warp_val
                        mx = j + rhoFactor * wave_magnitude * sin(i/period)
                        my = i + rhoFactor * wave_magnitude * sin(j/period)

                    elif warp_type == SWIRL_WARP:
                        strength = warp_val
                        # compute new angle coordinate
                        phiPrime = swirl_rotation + strength * exp(-1 * rho / swirlRadius) + phi
                        
                        # unwrap back into cartesian & compute distortion
                        dx = rho * cos(phiPrime) - x
                        dy = rho * sin(phiPrime) - y

                        # compute the map.
                        mx = j + rhoFactor * dx
                        my = i + rhoFactor * dy

                    else:
                        # technically should not get here.
                        #msg = 'Hit the default item in warp effect. Should not get here.'
                        #self.printError(msg)

                        mx = float(j)
                        my = float(i)

                    srcX[i, j] = mx
                    srcY[i, j] = my

        cur_warp_maps.append((srcX, srcY))

    return cur_warp_maps

if __name__ == "__main__":
    main()