""" This is the main dancewall class.
"""

import cv2
from .base import DWObject
from .effects import ColorEffect, EdgeEffect, WarpEffect, CartoonEffect
from time import time
import random

MAX_CAMERAS = 2
EFFECT_TYPES = ['colorEffect', 'edgeEffect', 'warpEffect', 'cartoonEffect']
RANDOM_EFFECT_TIME_LIMIT = 60.0
RANDOM_GRID_EFFECT_INTERVAL = 3

class DanceWall(DWObject):
    """Main class that controls the camera and Dance Wall effects.
    """
    def __init__(self, debug=False):
        super().__init__(debug)

        # create dictonary of effects.
        self.effects = {}
        self.effects['colorEffect'] = ColorEffect()
        self.effects['edgeEffect'] = EdgeEffect()
        self.effects['warpEffect'] = WarpEffect()
        self.effects['cartoonEffect'] = CartoonEffect()
        self.currentEffect = 'colorEffect'

        # initialize objects.
        self.printDebugMsg('Opening opencv objects.')
        self.cameraID = 0
        self.camera = cv2.VideoCapture(self.cameraID)

        # image objects.
        self.inFrame = None
        self.outFrame = None

        # set-up the random effect.
        self.do_random = False
        self.num_random_changes = 0
        self.last_change_time = time()
        random.seed(self.last_change_time)

    def __del__(self):
        """ Clean up the opencv objects
        """
        self.printDebugMsg('Closing opencv objects.')
        self.camera.release()
        cv2.destroyAllWindows()

    def init(self):
        """Initialize after the object is instantiated.
            Allows us to check for proper camera initialization.
        """
        print('Hello Dance Wall World!')

        if not self.camera.isOpened():
            msg = 'Cannot open the video camera.'
            self.printError(msg)
            return False

        # get the width and height of the video frame
        dWidth = self.camera.get(cv2.CAP_PROP_FRAME_WIDTH)
        dHeight = self.camera.get(cv2.CAP_PROP_FRAME_HEIGHT)

        print('Initial Frame size: {0} x {1}'.format(dWidth, dHeight))

        # set-up the window.
        cv2.namedWindow('DanceWall', cv2.WINDOW_NORMAL)
        cv2.setWindowProperty('DanceWall', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

        return True

    def changeCamera(self):
        """Change the camera being used.
        """
        self.cameraID += 1
        if self.cameraID > MAX_CAMERAS:
            self.cameraID = 0

        cv2.destroyAllWindows()
        self.camera.open(self.cameraID)
        return self.init()

    def updateFrame(self):
        """This function will update the frame to be printed to screen.
        """
        bSuccess, self.inFrame = self.camera.read()

        if not bSuccess:
            print('Cannot read a frame from the video stream.')
            return False

        if self.do_random:
            duration = time() - self.last_change_time
            if duration > RANDOM_EFFECT_TIME_LIMIT:
                self.doNextRandom()

        # flip image from webcam so it aligns with kids movements.
        self.inFrame = cv2.flip(self.inFrame, 1)

        self.effects[self.currentEffect].addNewFrame(self.inFrame)
        self.effects[self.currentEffect].drawEffect()
        self.outFrame = self.effects[self.currentEffect].getOutFrame()

        #broadcast.
        cv2.imshow('DanceWall', self.outFrame)

        return True

    def onKeyPress(self, key):
        """ Function called when key press is detected in main loop.
        """
        # local variables
        effect = self.currentEffect
        bSuccess = True
        bUnknown = False
        bParm = False

        # check the key being pressed.
        if key & 0xFF == ord('1'):
            print('ColorEffect is selected.')
            effect = 'colorEffect'

        elif key & 0xFF == ord('2'):
            print('EdgeEffect is selected.')
            effect = 'edgeEffect'

        elif key & 0xFF == ord('3'):
            print('WarpEffect is selected.')
            effect = 'warpEffect'

        elif key & 0xFF == ord('4'):
            print('CartoonEffect is selected.')
            effect = 'cartoonEffect'

        elif key & 0xFF == ord('c'):
            print('Change camera.')
            self.changeCamera()

        elif key & 0xFF == ord('m'):
            print('ColorMap increment is selected.')
            self.effects[self.currentEffect].changeColorMap()
            bParm = True

        elif key & 0xFF == ord('o'):
            print('ColorMap turned on is selected.')
            self.effects[self.currentEffect].toggleColorMapsOn()
            bParm = True

        elif key & 0xFF == ord('g'):
            print('Grid Order increment is selected.')
            self.effects[self.currentEffect].incrementGridOrder()
            bParm = True

        elif key & 0xFF == ord('r'):
            print('Toggle random effects on/off. Set to {0}'.format(not self.do_random))
            self.do_random = not self.do_random
            bParm = True

        elif (key & 0xFF == ord('q')) or (key == 27):
            print('An exit key was pressed.')
            bSuccess = False

        else:
            msg = 'An unknown key was pressed: {0}'.format(chr(key))
            print(msg)
            bUnknown = True

        # check whether an action shall be completed.
        if bSuccess and (not bUnknown) and (not bParm):
            if self.currentEffect == effect:
                self.effects[self.currentEffect].togglePresets()
            else:
                self.currentEffect = effect
                self.effects[self.currentEffect].init()


        return bSuccess

    def doNextRandom(self):
        """ Internal function when it is time to change the effect.
        """
        # get the random grid effect.
        self.currentEffect = random.choice(EFFECT_TYPES)

        # turn on grid?
        self.num_random_changes += 1
        doGrid = not bool(self.num_random_changes % RANDOM_GRID_EFFECT_INTERVAL)

        print("num changes: {0}  -- doGrid: {1}".format(self.num_random_changes, doGrid))

        self.effects[self.currentEffect].init()
        print('getRandomConfig should be called next...')
        self.effects[self.currentEffect].getRandomConfig(doGrid)

        # reset the start time.
        self.last_change_time = time()


