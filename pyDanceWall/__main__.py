"""This is the main entry point into the pyDanceWall module.
"""
import sys
import platform
#import numpy as np
import cv2
import os

from .dancewall import DanceWall


def main(args=None):
    """The main routine."""
    if args is None:
        args = sys.argv[1:]

    print("Running pyDanceWall with the following args {0}.".format(args))
    print()
    print("pyDanceWall is running on {0}".format(platform.platform()))
    print()
    print("pyDanceWall is located here: {0}".format(os.path.dirname(__file__)))

    # Do argument parsing here (eg. with argparse) and anything else
    # you want your project to do.

    # local variables.
    key = -1
    bSuccess = True

    dw = DanceWall()
    if not dw.init():
        print('Failed to init camera. Exiting.')
        return -1

    while bSuccess:
        # update the current frame.
        dw.updateFrame()

        key = cv2.waitKey(1)
        if key >= 0:
            bSuccess = dw.onKeyPress(key)

    print('Exiting normally.')
    del dw
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
    