import os
import sys

from setuptools import setup

from pyDanceWall import __version__ as version

if not version:
    raise RuntimeError('Cannot find version information')

packages = ['pyDanceWall']
requires = []
test_requirements = []

with open('README.md', 'r') as f:
    readme = f.read()

setup(
    name                 = 'pyDanceWall',
    version              = version,
    description          = 'Fun interactive video for kids.',
    long_description     = readme,
    author               = 'Andrew Wiles',
    author_email         = 'adwiles@gmail.com',
    url                  = 'tbd',
    packages             = packages,
    package_data         = {},
    package_dir          = {'pyDanceWall': 'pyDanceWall'},
    entry_points         = {'console_scripts': ['pyDanceWall = pyDanceWall.__main__:main'] },
    include_package_data = True,
    install_requires     = requires,
    license              = 'Copyright (C) 2018, Andrew Wiles. All rights reserved.',
    zip_safe             = False,
    cmdclass             = {},
    tests_require        = test_requirements,
    extras_require       = {},
    classifiers = (
        'Development Status :: Stable',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython')
)
