"""Tests of pyDanceWall

"""
import unittest
import time

import pyDanceWall
from pyDanceWall.base import DWObject
from pyDanceWall.dancewall import DanceWall
import cv2

print('pyDanceWall imported from "{path}"\n'.format(path=pyDanceWall.__path__[0]))

class TestDWObject(unittest.TestCase):
	def test_debug(self):
		obj = DWObject()

		self.assertFalse(obj.debug, 'Debug should be off by default')
		
		obj.turnOffDebug()
		self.assertFalse(obj.debug, 'Debug should have been turned off.')

		obj.turnOnDebug()
		self.assertTrue(obj.debug, 'Debug should have been turned on.')

		obj.toggleDebug()
		self.assertFalse(obj.debug, 'Debug should have been toggled off.')

		obj.toggleDebug()
		self.assertTrue(obj.debug, 'Debug should have been toggled on.')

	def test_print_debug_msg(self):
		obj = DWObject()
		obj.turnOnDebug()
		obj.printDebugMsg('This is testing the printing of a debug message to screen.')

	def test_print_error_msg(self):
		obj = DWObject()
		obj.printError('This is testing the printing of an error message to screen.')

	def test_debug_image(self):
		# load the test image.

		img = cv2.imread('./images/collina.jpg')
		self.assertIsNotNone(img, 'The image read in should not be None.')

		obj = DWObject(True)
		obj.showDebugImage('Collina', img)
		time.sleep(2)

class TestDanceWall(unittest.TestCase):
	def test_camera_initialized(self):
		dw = DanceWall(True)
		self.assertTrue((type(dw.camera) is cv2.VideoCapture), 'Camera should be initialized as VideoCapture object')

		dw.init()
		for i in range(5):
			dw.updateFrame()

		del dw
		cv2.destroyAllWindows()


if __name__ == '__main__':
	unittest.main()