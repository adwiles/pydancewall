# pyDanceWall

This is a fun interactive Dance Wall for kids.  The application requires a webcam and TV or projector it will process the video to create some fun interactive views.

## Installation

pyDanceWall runs on [Python][python] 3.6+ and depends on the following packages:

- [OpenCV][opencv-python]

can be installed by running the following command:

```
$ pip install git+https://adwiles@bitbucket.org/adwiles/pydancewall.git
```

You can easily upgrade with the following command:

```
$ pip install --upgrade git+https://adwiles@bitbucket.org/adwiles/pydancewall.git
```

If you wish to develop this package (i.e. work on a custom branch) you can use the following:

```
$ git clone https://adwiles@bitbucket.org/adwiles/pydancewall.git
$ pip install -e ./pydancewall
$ cd pydancewall/test
$ python -m unittest -v test.py
```

To uninstall simply execute:

```
$ pip uninstall pyDanceWall
```

## Using pyDanceWall

```
$ python -m pyDanceWall
```

Have fun!
